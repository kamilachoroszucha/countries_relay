const webpack = require('webpack');

const path = require('path');
const RelayCompilerWebpackPlugin = require('relay-compiler-webpack-plugin');


module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(ttf|eot|otf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {loader: "file-loader"}
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          }
        ]
      }
    ],

        
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', ".webpack.js", ".web.js", ".mjs", ".json"]
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new RelayCompilerWebpackPlugin({
        schema: path.resolve(__dirname, './schema.graphql'), // or schema.json
        src: path.resolve(__dirname, './src'),
      })
  ],
  devServer: {
    contentBase: './dist',
    hot: true
  },
  node: {
    fs: 'empty'
  }
};