const getCountryDataQuery = graphql`
  query getCountryDataQuery($id: String) {
    country(code: $id) {
      name
      phone
      native
      currency
    }
  }
`;

export default getCountryDataQuery;
