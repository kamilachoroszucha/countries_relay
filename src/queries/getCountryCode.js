const getCountryCodeQuery = graphql`
query getCountryCodeQuery{
    countries{
        code
        name
      
  }
}`

export default getCountryCodeQuery;