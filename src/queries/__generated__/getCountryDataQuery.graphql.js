/**
 * @flow
 * @relayHash 7675fbfd28c2fa57df671576f24e867b
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type getCountryDataQueryVariables = {|
  id?: ?string
|};
export type getCountryDataQueryResponse = {|
  +country: ?{|
    +name: ?string,
    +phone: ?string,
    +native: ?string,
    +currency: ?string,
  |}
|};
export type getCountryDataQuery = {|
  variables: getCountryDataQueryVariables,
  response: getCountryDataQueryResponse,
|};
*/


/*
query getCountryDataQuery(
  $id: String
) {
  country(code: $id) {
    name
    phone
    native
    currency
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "String",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "country",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "code",
        "variableName": "id"
      }
    ],
    "concreteType": "Country",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "phone",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "native",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "currency",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "getCountryDataQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "getCountryDataQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "getCountryDataQuery",
    "id": null,
    "text": "query getCountryDataQuery(\n  $id: String\n) {\n  country(code: $id) {\n    name\n    phone\n    native\n    currency\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'a4d9119ab833a17e8afb30ba3a5accd5';
module.exports = node;
