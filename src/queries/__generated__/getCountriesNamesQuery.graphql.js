/**
 * @flow
 * @relayHash d3a33272c3f3be46f032a6f9529ddf15
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type getCountriesNamesQueryVariables = {|
  id?: ?string
|};
export type getCountriesNamesQueryResponse = {|
  +continent: ?{|
    +countries: ?$ReadOnlyArray<?{|
      +name: ?string
    |}>
  |},
  +notes: ?string,
|};
export type getCountriesNamesQuery = {|
  variables: getCountriesNamesQueryVariables,
  response: getCountriesNamesQueryResponse,
|};
*/


/*
query getCountriesNamesQuery(
  $id: String
) {
  continent(code: $id) {
    countries {
      name
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "String",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "continent",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "code",
        "variableName": "id"
      }
    ],
    "concreteType": "Continent",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "countries",
        "storageKey": null,
        "args": null,
        "concreteType": "Country",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  },
  {
    "kind": "ClientExtension",
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "notes",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "getCountriesNamesQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "getCountriesNamesQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "getCountriesNamesQuery",
    "id": null,
    "text": "query getCountriesNamesQuery(\n  $id: String\n) {\n  continent(code: $id) {\n    countries {\n      name\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'b41bb33f446b528d739ffddd782d51dd';
module.exports = node;
