/**
 * @flow
 * @relayHash be909c105249e3fc4487e5af4c51a022
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type getContinentCodeQueryVariables = {||};
export type getContinentCodeQueryResponse = {|
  +continents: ?$ReadOnlyArray<?{|
    +code: ?string,
    +name: ?string,
  |}>
|};
export type getContinentCodeQuery = {|
  variables: getContinentCodeQueryVariables,
  response: getContinentCodeQueryResponse,
|};
*/


/*
query getContinentCodeQuery {
  continents {
    code
    name
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "continents",
    "storageKey": null,
    "args": null,
    "concreteType": "Continent",
    "plural": true,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "code",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "getContinentCodeQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "getContinentCodeQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "getContinentCodeQuery",
    "id": null,
    "text": "query getContinentCodeQuery {\n  continents {\n    code\n    name\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'a013bb02fe601ec87b84f57807840e8b';
module.exports = node;
