/**
 * @flow
 * @relayHash b590cb992968417dae38d89ed7ef754f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type getCountryCodeQueryVariables = {||};
export type getCountryCodeQueryResponse = {|
  +countries: ?$ReadOnlyArray<?{|
    +code: ?string,
    +name: ?string,
  |}>
|};
export type getCountryCodeQuery = {|
  variables: getCountryCodeQueryVariables,
  response: getCountryCodeQueryResponse,
|};
*/


/*
query getCountryCodeQuery {
  countries {
    code
    name
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "countries",
    "storageKey": null,
    "args": null,
    "concreteType": "Country",
    "plural": true,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "code",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "name",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "getCountryCodeQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "getCountryCodeQuery",
    "argumentDefinitions": [],
    "selections": (v0/*: any*/)
  },
  "params": {
    "operationKind": "query",
    "name": "getCountryCodeQuery",
    "id": null,
    "text": "query getCountryCodeQuery {\n  countries {\n    code\n    name\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '0d889ad8016d8a4ba567a6a4d7f863e7';
module.exports = node;
