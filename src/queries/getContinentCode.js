const getContinentCodeQuery = graphql`
query getContinentCodeQuery{
    continents{
        code
        name
      
  }
}`

export default getContinentCodeQuery;