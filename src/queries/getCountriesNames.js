const getCountriesNamesQuery = graphql`
  query getCountriesNamesQuery($id: String) {
    continent(code: $id) {
      countries {
        name
      }
    }
    notes
  }
`;

export default getCountriesNamesQuery;
