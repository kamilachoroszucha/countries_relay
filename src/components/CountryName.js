import React from "react";
//import { createFragmentContainer } from "react-relay";
import { useFragment, graphql } from "react-relay/hooks";
// class CountryName extends React.Component {
//     render() {
//         const {name} = this.props.country

//       return<>{name}</>
//     }
//   }

// export default createFragmentContainer(CountryName,{
//     country:graphql`
//     fragment CountryName_country on Country{
//         name
//     }
//     `
// })

const CountryName = props => {
  const countryNameData = useFragment(
    graphql`
      fragment CountryName_country on Country {
        name
      }
    `,
    props.country
  );
  return (
    <div>
      <h2>{countryNameData.name}</h2>
    </div>
  );
};

export default CountryName;
