import React from "react";
import { useFragment, graphql } from "react-relay/hooks";

const fragmentNew = graphql`
  fragment NewFragment_country on Country {
    languages {
      name
    }
  }
`;
const NewFragment = props => {
  const language = useFragment(fragmentNew, props.country);
  return <p>{language.languages[0].name}</p>;
};

export default NewFragment;
