import React from "react";
import { useFragment, graphql } from "react-relay/hooks";

const newFragment = graphql`
  fragment OtherCountryInfo_country on Country {
    currency
    native
    languages {
      native
    }
  }
`;

const OtherCountryInfo = props => {
  const additionalInfo = useFragment(newFragment, props.country);
  return (
    <div>
      <p>{additionalInfo.currency}</p>
      <p>{additionalInfo.native}</p>
      <p>{additionalInfo.languages[0].native}</p>
    </div>
  );
};

export default OtherCountryInfo;
