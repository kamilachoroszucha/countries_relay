import React, { useState } from "react";
import FormInput from "./FormInput";
import CountriesCheck from "./CountriesCheck";
import RelayEnvironment from "../RelayEnvironment";
import getCountryCodeQuery from "../queries/getCountryCode";
import { preloadQuery } from "react-relay/hooks";
import { Link } from "react-router-dom";

const Countries = () => {
  const [input, setInput] = useState("");
  const [userInput, setUserInput] = useState("");
  const handleSubmit = event => {
    event.preventDefault();
    setUserInput(input);
  };
  const handleOnChange = event => setInput(event.target.value);
  return (
    <>
      <button className="back__button">
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>
          BACK
        </Link>
      </button>
      <h2>Find out more about countries</h2>

      <FormInput
        subject={`country`}
        handleSubmit={handleSubmit}
        input={input}
        handleOnChange={handleOnChange}
      />
      <CountriesCheck
        userInput={userInput}
        preloadedQuery={preloadQuery(RelayEnvironment, getCountryCodeQuery)}
      />
    </>
  );
};

export default Countries;
