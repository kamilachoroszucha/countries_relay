import React from "react";
import RelayEnvironment from "../RelayEnvironment";
import { preloadQuery, usePreloadedQuery } from "react-relay/hooks";
import getCountryCodeQuery from "../queries/getCountryCode";
import getCountryDataQuery from "../queries/getCountryData";

const CountriesCheck = ({ userInput, preloadedQuery }) => {
  const data = usePreloadedQuery(getCountryCodeQuery, preloadedQuery);
  const allCodes = data.countries;
  const chosenCountry =
    userInput &&
    allCodes.find((item)=>item.name==userInput)

  const newData = usePreloadedQuery(
    getCountryDataQuery,
    preloadQuery(RelayEnvironment, getCountryDataQuery, {
      id: chosenCountry.code
    })
  );

  return (
    <>
      {newData.country.name !== null && (
        <div className="countryInfo">
          <p className="countryInfo__paragraph">You've picked:</p>{" "}
          <p className="countryInfo__data">{newData.country.name}</p>
          <p className="countryInfo__paragraph">Currency of this country is:</p>
          <p className="countryInfo__data"> {newData.country.currency}</p>
          <p className="countryInfo__paragraph">
            Phone numbers in this country begin with:
          </p>
          <p className="countryInfo__data"> {newData.country.phone}</p>
          <p className="countryInfo__paragraph">This Country native is:</p>
          <p className="countryInfo__data"> {newData.country.native}</p>
        </div>
      )}
    </>
  );
};

export default CountriesCheck;
