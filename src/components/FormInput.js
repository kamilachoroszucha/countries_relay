import React, { useState, useRef, useEffect, useContext } from "react";
import ModeContext from "../context/ModeContext";

const FormInput = ({ handleOnChange, input, handleSubmit, subject }) => {
  const theme = useContext(ModeContext);
  const [visible, setVisible] = useState(false);
  const continentInput = useRef();
  useEffect(() => {
    visible && continentInput.current.focus();
  });

  return (
    <>
      <button
        className={`form__button form__button--search + ${
          theme ? null : `lightFontColor`
        }`}
        onClick={() => setVisible(prev => !prev)}
      >
        Search for a {subject}
      </button>
      <form
        style={{ display: visible ? "block" : "none" }}
        className="form"
        onSubmit={handleSubmit}
      >
        <label className="form__label">
          Enter chosen {subject}
          <br />
          <input
            ref={continentInput}
            className="form__input"
            type="text"
            value={input}
            onChange={handleOnChange}
          ></input>
        </label>
        <button className={`form__button + ${theme ? null : `lightFontColor`}`}>
          Find out
        </button>
      </form>
    </>
  );
};
export default FormInput;
