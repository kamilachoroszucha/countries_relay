import React from "react";
import CountryName from "./CountryName";
import { Link } from "react-router-dom";
import { preloadQuery, usePreloadedQuery } from "react-relay/hooks";
import { graphql } from "react-relay";
import RelayEnvironment from "../RelayEnvironment";
import OtherCountryInfo from "./OtherCountryInfo";
import NewFragment from "./NewFragment";
import getCountryCodeQuery from "./../queries/getCountryCode";

const ForFragmentQuery = graphql`
  query ForFragmentQuery($id: String) {
    country(code: $id) {
      ...CountryName_country
      ...OtherCountryInfo_country
      ...NewFragment_country
    }
  }
`;

const ForFragment = () => {
  const favouriteCountry = localStorage.getItem("userInput");

  const countriesCodes = usePreloadedQuery(
    getCountryCodeQuery,
    preloadQuery(RelayEnvironment, getCountryCodeQuery)
  ).countries;

  const chosenCountry =
    favouriteCountry &&
    countriesCodes.find(item => item.name == favouriteCountry);

  const newData = usePreloadedQuery(
    ForFragmentQuery,
    preloadQuery(RelayEnvironment, ForFragmentQuery, {
      id: chosenCountry.code
    })
  );
  const country = newData.country;
  return (
    <>
      <button className="back__button">
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>
          BACK
        </Link>
      </button>
      {favouriteCountry ? (
        <>
          <CountryName country={country} />
          <OtherCountryInfo country={country} />
          <NewFragment country={country} />
        </>
      ) : (
        `Go to Continents section first!`
      )}
    </>
  );
};

export default ForFragment;
