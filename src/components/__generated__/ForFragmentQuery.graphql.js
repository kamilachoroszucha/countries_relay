/**
 * @flow
 * @relayHash abd436f9d9dc523684405228945d9f1f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type CountryName_country$ref = any;
type NewFragment_country$ref = any;
type OtherCountryInfo_country$ref = any;
export type ForFragmentQueryVariables = {|
  id?: ?string
|};
export type ForFragmentQueryResponse = {|
  +country: ?{|
    +$fragmentRefs: CountryName_country$ref & OtherCountryInfo_country$ref & NewFragment_country$ref
  |}
|};
export type ForFragmentQuery = {|
  variables: ForFragmentQueryVariables,
  response: ForFragmentQueryResponse,
|};
*/


/*
query ForFragmentQuery(
  $id: String
) {
  country(code: $id) {
    ...CountryName_country
    ...OtherCountryInfo_country
    ...NewFragment_country
  }
}

fragment CountryName_country on Country {
  name
}

fragment NewFragment_country on Country {
  languages {
    name
  }
}

fragment OtherCountryInfo_country on Country {
  currency
  native
  languages {
    native
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "String",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "code",
    "variableName": "id"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "native",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "ForFragmentQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "country",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "Country",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "CountryName_country",
            "args": null
          },
          {
            "kind": "FragmentSpread",
            "name": "OtherCountryInfo_country",
            "args": null
          },
          {
            "kind": "FragmentSpread",
            "name": "NewFragment_country",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ForFragmentQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "country",
        "storageKey": null,
        "args": (v1/*: any*/),
        "concreteType": "Country",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "currency",
            "args": null,
            "storageKey": null
          },
          (v3/*: any*/),
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "languages",
            "storageKey": null,
            "args": null,
            "concreteType": "Language",
            "plural": true,
            "selections": [
              (v3/*: any*/),
              (v2/*: any*/)
            ]
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "ForFragmentQuery",
    "id": null,
    "text": "query ForFragmentQuery(\n  $id: String\n) {\n  country(code: $id) {\n    ...CountryName_country\n    ...OtherCountryInfo_country\n    ...NewFragment_country\n  }\n}\n\nfragment CountryName_country on Country {\n  name\n}\n\nfragment NewFragment_country on Country {\n  languages {\n    name\n  }\n}\n\nfragment OtherCountryInfo_country on Country {\n  currency\n  native\n  languages {\n    native\n  }\n}\n",
    "metadata": {}
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '6d76019316f8b4cb9b09c4fc59b72731';
module.exports = node;
