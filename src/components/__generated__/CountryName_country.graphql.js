/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type CountryName_country$ref: FragmentReference;
declare export opaque type CountryName_country$fragmentType: CountryName_country$ref;
export type CountryName_country = {|
  +name: ?string,
  +$refType: CountryName_country$ref,
|};
export type CountryName_country$data = CountryName_country;
export type CountryName_country$key = {
  +$data?: CountryName_country$data,
  +$fragmentRefs: CountryName_country$ref,
};
*/


const node/*: ReaderFragment*/ = {
  "kind": "Fragment",
  "name": "CountryName_country",
  "type": "Country",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "name",
      "args": null,
      "storageKey": null
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '940d69c83379536c9bed3b331032f5a9';
module.exports = node;
