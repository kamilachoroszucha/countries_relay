/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type OtherCountryInfo_country$ref: FragmentReference;
declare export opaque type OtherCountryInfo_country$fragmentType: OtherCountryInfo_country$ref;
export type OtherCountryInfo_country = {|
  +currency: ?string,
  +native: ?string,
  +languages: ?$ReadOnlyArray<?{|
    +native: ?string
  |}>,
  +$refType: OtherCountryInfo_country$ref,
|};
export type OtherCountryInfo_country$data = OtherCountryInfo_country;
export type OtherCountryInfo_country$key = {
  +$data?: OtherCountryInfo_country$data,
  +$fragmentRefs: OtherCountryInfo_country$ref,
};
*/


const node/*: ReaderFragment*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "native",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Fragment",
  "name": "OtherCountryInfo_country",
  "type": "Country",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "currency",
      "args": null,
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "languages",
      "storageKey": null,
      "args": null,
      "concreteType": "Language",
      "plural": true,
      "selections": [
        (v0/*: any*/)
      ]
    }
  ]
};
})();
// prettier-ignore
(node/*: any*/).hash = '2bb12c4a687df279b10dcce837515e10';
module.exports = node;
