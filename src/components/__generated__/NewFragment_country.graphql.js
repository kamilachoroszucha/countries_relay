/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type NewFragment_country$ref: FragmentReference;
declare export opaque type NewFragment_country$fragmentType: NewFragment_country$ref;
export type NewFragment_country = {|
  +languages: ?$ReadOnlyArray<?{|
    +name: ?string
  |}>,
  +$refType: NewFragment_country$ref,
|};
export type NewFragment_country$data = NewFragment_country;
export type NewFragment_country$key = {
  +$data?: NewFragment_country$data,
  +$fragmentRefs: NewFragment_country$ref,
};
*/


const node/*: ReaderFragment*/ = {
  "kind": "Fragment",
  "name": "NewFragment_country",
  "type": "Country",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "languages",
      "storageKey": null,
      "args": null,
      "concreteType": "Language",
      "plural": true,
      "selections": [
        {
          "kind": "ScalarField",
          "alias": null,
          "name": "name",
          "args": null,
          "storageKey": null
        }
      ]
    }
  ]
};
// prettier-ignore
(node/*: any*/).hash = '20a2f508b281e9630d9ac99e9a0d2f81';
module.exports = node;
