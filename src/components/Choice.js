import React, { useContext } from "react";
import { Link } from "react-router-dom";
import ModeContext from "../context/ModeContext";

const Choice = () => {
  const theme = useContext(ModeContext);
  return (
    <>
      <h1 className="choice">Let's repeat geography!</h1>
      <p className="choice__paragraph">What would you like to know about?</p>

      <button className="choice__button choice__continents">
        <Link
          to="/continents"
          style={{ textDecoration: "none", color: theme ? `black` : `#eeeeee` }}
        >
          CONTINENTS
        </Link>
      </button>
      <button className="choice__button choice__countries">
        <Link
          to="/countries"
          style={{ textDecoration: "none", color: theme ? `black` : `#eeeeee` }}
        >
          COUNTRIES
        </Link>
      </button>
      <button className="choice__button choice__fragment">
        <Link
          to="/forfragment"
          style={{ textDecoration: "none", color: "black" }}
        >
          FAVOURITE COUNTRY
        </Link>
      </button>
    </>
  );
};

export default Choice;
