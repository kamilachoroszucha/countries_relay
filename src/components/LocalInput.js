import React, { useState, useRef, useEffect, useContext } from "react";
import { commitLocalUpdate } from "relay-runtime";
import RelayEnvironment from "../RelayEnvironment";
import ModeContext from "../context/ModeContext";

function LocalInput() {
  const [userInput, setUserInput] = useState("");
  const theme = useContext(ModeContext);
  const countryInput = useRef();
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    visible && countryInput.current.focus();
  });
  const handleChange = e => {
    setUserInput(e.target.value);
  };
  const handleClick = e => {
    e.preventDefault();
    commitLocalUpdate(RelayEnvironment, store => {
      store.getRoot().setValue(userInput, "notes");
    });
    localStorage.setItem("userInput", userInput);
  };
  return (
    <>
      <button
        className={`form__button form__button--search + ${
          theme ? null : `lightFontColor`
        }`}
        onClick={() => setVisible(prev => !prev)}
      >
        Search for a country
      </button>

      <form style={{ display: visible ? "block" : "none" }}>
        <label className="localInput">
          What country would you like to visit the most?
          <input
            ref={countryInput}
            className="localInput__input"
            type="text"
            onChange={handleChange}
          ></input>
        </label>
        <button onClick={handleClick}>Confirm</button>
      </form>
    </>
  );
}

export default LocalInput;
