import React from "react";
import getContinentCodeQuery from "../queries/getContinentCode";
import RelayEnvironment from "../RelayEnvironment";
import { preloadQuery, usePreloadedQuery } from "react-relay/hooks";
import getCountriesNamesQuery from "../queries/getCountriesNames";

function CodeChecker({ userInput, preloadedQuery }) {
  const data = usePreloadedQuery(getContinentCodeQuery, preloadedQuery);
  const allCodes = data.continents;
  const chosenContinent =
    userInput && allCodes.find(item => item.name == userInput);
  const newData = usePreloadedQuery(
    getCountriesNamesQuery,
    preloadQuery(RelayEnvironment, getCountriesNamesQuery, {
      id: chosenContinent.code
    })
  );
  const note = newData.notes;
  const countriesOnCountinent = newData.continent.countries;

  const chosenCountry = countriesOnCountinent.filter(function(item) {
    return item.name.includes(note);
  })[0];

  return (
    <div className="output">
      {chosenCountry !== undefined ? (
        <p className="output__paragraph">
          Hooray! Your dreamed country is on this continent!
        </p>
      ) : null}
      <ul className="output__list">
        {newData.continent.countries.map((value, index) => {
          return (
            <li className="output__list--item" key={index}>
              {value.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default CodeChecker;
