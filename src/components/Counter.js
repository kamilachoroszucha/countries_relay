import React, { useState, useEffect } from "react";

const Counter = () => {
  const [secondsElapsed, setSecondsElapsed] = useState(0);
  useEffect(() => {
    const intervalID = setInterval(
      () => setSecondsElapsed(prev => prev + 1),
      1000
    );
    return () => {
      clearInterval(intervalID);
    };
  }, []);
  const minutes = Math.floor(secondsElapsed / 60);
  const seconds = Math.floor(secondsElapsed % 60);

  return (
    <div className="App__clock">
      You've been learning <br />
      geography for {minutes} minutes {seconds} seconds.
    </div>
  );
};

export default Counter;
