import { useState, useEffect } from "react";

const useTimeOnPage = () => {
  const [secondsElapsed, setSecondsElapsed] = useState(0);
  useEffect(() => {
    const intervalID = setInterval(
      () => setSecondsElapsed(prev => prev + 1),
      1000
    );
    return () => {
      clearInterval(intervalID);
    };
  }, []);
  return secondsElapsed;
};

export default useTimeOnPage;
