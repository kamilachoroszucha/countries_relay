import React, { useState } from "react";
import CodeChecker from "./CodeChecker";
import getContinentCodeQuery from "../queries/getContinentCode";
import FormInput from "./FormInput";
import LocalInput from "./LocalInput";
import { preloadQuery } from "react-relay/hooks";
import RelayEnvironment from "../RelayEnvironment";
import { Link } from "react-router-dom";

function MainContinents() {
  const [input, setInput] = useState("");
  const [userInput, setUserInput] = useState("");
  const handleSubmit = event => {
    event.preventDefault();
    setUserInput(input);
  };
  const handleOnChange = event => setInput(event.target.value);
  return (
    <div className="ContinentApp">
      <button className="back__button">
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>
          BACK
        </Link>
      </button>
      <header className="App__header">
        <h2 className="App__header--heading">
          Find out what countries are on each continent!
        </h2>
        <p>...and if your dreamed country is there!</p>
      </header>
      <LocalInput />
      <br />
      <FormInput
        subject={`continent`}
        handleSubmit={handleSubmit}
        input={input}
        handleOnChange={handleOnChange}
      />{" "}
      <CodeChecker
        userInput={userInput}
        preloadedQuery={preloadQuery(RelayEnvironment, getContinentCodeQuery)}
      />
    </div>
  );
}

export default MainContinents;
