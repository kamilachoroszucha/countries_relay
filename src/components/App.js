"use strict";

import React, { useState } from "react";
import { RelayEnvironmentProvider } from "react-relay/hooks";
import RelayEnvironment from "../RelayEnvironment";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Choice from "./Choice";
import MainContinents from "./MainContinents";
import Countries from "./Countries";
import ForFragment from "./ForFragment";
import Counter from "./Counter";
import ModeContext from "../context/ModeContext";

const { Suspense } = React;

function App() {
  const [theme, setTheme] = useState(true);
  const handleClick = () => {
    setTheme(prev => !prev);
  };

  return (
    <ModeContext.Provider value={theme}>
      <div className={`App + ${theme ? `light` : `dark`}`}>
        <input
          onClick={handleClick}
          type="checkbox"
          id="toggle"
          className="checkbox"
        />
        <label htmlFor="toggle" className="switch"></label>
        <Counter />
        <Router>
          <Switch>
            <Route exact path="/" component={Choice} />
            <Route exact path="/continents" component={MainContinents} />
            <Route exact path="/countries" component={Countries} />
            <Route exact path="/forfragment" component={ForFragment} />
          </Switch>
        </Router>
      </div>
    </ModeContext.Provider>
  );
}

function AppRoot() {
  return (
    <RelayEnvironmentProvider environment={RelayEnvironment}>
      <Suspense fallback={"Loading..."}>
        <App />
      </Suspense>
    </RelayEnvironmentProvider>
  );
}

export default AppRoot;
